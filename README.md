# device_board_m5stack

#### 介绍
个人移植OHOS适配M5Core2，M5Paper，m5Stickc_plus

#### 软件架构
软件架构说明

OpenHarmony mini + LVGL 8.3

#### 安装教程

按照Ohos官方适配的开发板NiobiU4的资料进行开发环境搭建

需要额外下载三方库代码放到third_party下  
git clone -b release/v8.3 https://github.com/lvgl/lvgl.git third_party/lvgl

删除原版esp的soc仓  
rm -rf device/soc/esp

下载代码  
git clone -b OpenHarmony-3.2-Release https://gitee.com/diemit/vendor_m5stack.git vendor/m5stack  
git clone -b OpenHarmony-3.2-Release https://gitee.com/diemit/device_board_m5stack.git device/board/m5stack  
git clone -b OpenHarmony-3.2-Release https://gitee.com/diemit/device_soc_esp.git device/soc/esp  

使用[hb set]命令选择m5core2  
使用[hb build -f]命令编译固件  

参照NiobiU4的资料进行固件烧录  

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

